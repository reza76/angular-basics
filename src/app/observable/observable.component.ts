import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.scss']
})
export class ObservableComponent implements OnInit {
  orderStatus: any;
  data1: Observable<any> | undefined;
  // data2: Observable<any> | undefined;

  constructor() {

  }

  ngOnInit(): void {
    this.data1 = new Observable(observer => {
      setTimeout(() => {
        observer.next('In Progress');
      }, 2000);

      setTimeout(() => {
        observer.next('Processing...');
      }, 4000);

      setTimeout(() => {
        observer.next('Completed');
      }, 6000);

      // error
      setTimeout(() => {
        observer.error();
      }, 8000);

      // complete the observable
      setTimeout(() => {
        observer.complete();
      }, 10000);

      setTimeout(() => {
        observer.next('After Completed');
      }, 12000);


    });

    this.data1.subscribe(value => {
      this.orderStatus = value;
    });

    this.data1.subscribe(value2 => {
      this.orderStatus = value2;
    });


    //   this.data2 = new Observable(observer => {
    //     setTimeout(() => {
    //       observer.next('In Progress');
    //     }, 2000);
    //
    //     setTimeout(() => {
    //       observer.next('Processing...');
    //     }, 4000);
    //
    //     setTimeout(() => {
    //       observer.next('Completed');
    //     }, 6000);
    //   });
    //
    //   this.data2.subscribe(value => {
    //     this.orderStatus = value;
    //   });
    //
    }

  }
