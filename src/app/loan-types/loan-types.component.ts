import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-loan-types',
  templateUrl: './loan-types.component.html',
  styleUrls: ['./loan-types.component.scss']
})
export class LoanTypesComponent implements OnInit {
  // addLoanTypesForm: FormGroup;
  addLoanTypesForm = new FormGroup({
    loanName: new FormControl(),
    loanType: new FormControl(),
    loanDescription: new FormControl(),
  });

  constructor() {
  }

  ngOnInit(): void {

  }

  // tslint:disable-next-line:typedef
  addLoanType() {
    console.log(this.addLoanTypesForm.value);
  }
}
