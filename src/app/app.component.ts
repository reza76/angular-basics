 import { Component } from '@angular/core';

 @Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-basics';

  successMsg = true;
  txtColor = 'blue';
  bgColor = 'yellow';
  username = 'reza76';
  dateExamp = Date.now();
  jsonExmp = {
      fName : 'Hasan',
      lName : 'Reza',
      address : 'cumilla'
    };
  contacts = [
    {
      fName : 'Hasan',
      lName : 'Reza',
      address : 'cumilla'
    },
    {
      fName : 'Abdullah',
      lName : 'Mamun',
      address : 'dhaka'
    },
    {
      fName : 'Rifat',
      lName : 'Hasib',
      address : 'cumilla'
    },
    {
      fName : 'ibrahim',
      lName : 'ome',
      address : 'feni'
    }
  ];

  isDarkTheme = true;
   // tslint:disable-next-line:typedef
  toggleTheme(){
    this.isDarkTheme = !this.isDarkTheme;
  }
}
