import {Injectable} from '@angular/core';
import {CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {SearchComponent} from './search/search.component';

@Injectable({
  providedIn: 'root'
})
export class UnsavedGuard implements CanDeactivate<SearchComponent> {
  // tslint:disable-next-line:typedef
  canDeactivate(component: SearchComponent) {
    if(component.isDirty){
      return window.confirm('Are you sure to leave the page');
    }
    return true;
  }


}
