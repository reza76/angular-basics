import {Component, OnInit} from '@angular/core';
import {ContactsService} from '../../contacts.service';

@Component({
  selector: 'app-view-contact',
  templateUrl: './view-contact.component.html',
  styleUrls: ['./view-contact.component.scss']
})
export class ViewContactComponent implements OnInit {

  constructor(public contactsService: ContactsService) {
  }

  contactList: any;

  ngOnInit(): void {
    this.contactsService.getContacts().subscribe(data => {
      this.contactList = data;
    });
  }

}
