import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ContactsService} from '../../contacts.service';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent implements OnInit {
  firstName = '';
  terms = false;
  msgTrue = false;

  constructor(public contactsService: ContactsService) {
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  addNewContact(form: any) {
    // const newFormData = {id: form.value.name, firstName: form.value.email, lastName: form.value.password};
    this.contactsService.createContact(form.value).subscribe(data => {
      console.log(data);
      this.msgTrue = true;
    });
  }

  // tslint:disable-next-line:typedef
  addContact(formValue: NgForm) {
    console.log(formValue.value);
  }
}
