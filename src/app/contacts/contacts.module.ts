import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddContactComponent } from './add-contact/add-contact.component';
import { EditContactComponent } from './edit-contact/edit-contact.component';
import { DeleteContactComponent } from './delete-contact/delete-contact.component';
import { ListContactComponent } from './list-contact/list-contact.component';
import { ViewContactComponent } from './view-contact/view-contact.component';
import {FormsModule} from '@angular/forms';
// import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [AddContactComponent, EditContactComponent, DeleteContactComponent, ListContactComponent, ViewContactComponent],
  imports: [
    CommonModule,
    FormsModule,
    // HttpClientModule,
  ]
})
export class ContactsModule { }
