import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoansComponent} from './loans/loans.component';
import {LoanTypesComponent} from './loan-types/loan-types.component';
import {AdminComponent} from './admin/admin.component';
import {SuperAdminGuard} from './super-admin.guard';
import {AdminManageComponent} from './admin-manage/admin-manage.component';
import {AdminEditComponent} from './admin-edit/admin-edit.component';
import {AdminDeleteComponent} from './admin-delete/admin-delete.component';
import {AdminAccessGuard} from './admin-access.guard';
import {SearchComponent} from './search/search.component';
import {UnsavedGuard} from './unsaved.guard';
import {AddContactComponent} from './contacts/add-contact/add-contact.component';
import {ContactsModule} from './contacts/contacts.module';
import {ObservableComponent} from './observable/observable.component';
import {ViewContactComponent} from './contacts/view-contact/view-contact.component';

let routes: Routes;
routes = [
  {
    path: 'loans',
    children: [
      {
        path: 'loan-type',
        component: LoanTypesComponent
      },
      {
        path: 'loan',
        component: LoansComponent
      }
    ]
  },
  {
    path: 'admin',
    canActivate: [SuperAdminGuard],
    children: [
      {
        path: '',
        component: AdminComponent,

      },
      {
        path: '',
        canActivateChild: [AdminAccessGuard],
        children: [
          {path: 'manage', component: AdminManageComponent},
          {path: 'edit', component: AdminEditComponent},
          {path: 'delete', component: AdminDeleteComponent},

        ]
      }
    ]
  },
  {
    path: 'search',
    component: SearchComponent,
    canDeactivate: [UnsavedGuard]
  },
  {
    path: 'contacts/add',
    component: AddContactComponent,
    // children: [
    //   {path: 'add', component: AddContactComponent }
    // ]
  },
  {
    path: 'contacts/view',
    component: ViewContactComponent,
    // children: [
    //   {path: 'add', component: AddContactComponent }
    // ]
  },
  {
    path: 'loan-type',
    component: LoanTypesComponent,
    // children: [
    //   {path: 'add', component: AddContactComponent }
    // ]
  },
  {
    path: 'observable',
    component: ObservableComponent ,
    // children: [
    //   {path: 'add', component: AddContactComponent }
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
