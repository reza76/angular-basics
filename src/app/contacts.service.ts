import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor(private httpClient: HttpClient) { }

  // tslint:disable-next-line:typedef
  getContacts(){
    return this.httpClient.get('http://localhost:3000/contacts');
  }

  // tslint:disable-next-line:typedef
  createContact(createResourse: any){
    const httpHeaders = new HttpHeaders();
    httpHeaders.append('Content-Type', 'application/json');
    return this.httpClient.post('http://localhost:3000/contacts', createResourse, {headers: httpHeaders});
  }
}
